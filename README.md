# My Linux PC

Enjoy some pics & diagrams on my custom Ubuntu machine and everything I've done with it!

### Atlas Node

This documentation is also inside the `doc` project of the Atlas Node group.

https://gitlab.com/atlas-node

The Atlas Node group is comprised of many IaaS projects in which I use my 
personal Desktop in my office to lay down infrastructure on my Ubuntu machine 
over SSH, which runs in my basement with no screens or keyboards plugged into it.

The Ubuntu machine is essentially a Linux server, and the Atlas Node is all of the 
infrastructure I've laid down on it.

## Hardware

| Component     | Model     |
|:--------------|:----------|
| Motherboard | Gigabyte B450M DS3H |
| RAM | Corsair Vengeance LPX 16GB |
| CPU | AMD Ryzen 3 1200 |
| CPU Cooler | *Fan came with CPU |
| Power Supply | ARESGAME 500W Power Supply |
| GPU | AMD Radeon 6600 XT |
| Storage | WD Black Series 500 GB HDD |

## OS

Ubuntu 20.04

### To install Ubuntu Linux on a PC (new builds):
https://www.youtube.com/watch?v=X_fDdUgqIUQ
1. Download Ubuntu https://ubuntu.com/download/desktop
2. Download Rufus https://rufus.ie/en/
3. Insert a USB storage device and use Rufus to turn it into an Ubuntu bootable
4. Plug the USB into your new PC and turn it on
5. Select Ubuntu
6. You can choose to try it first or install it - eventually you want to install it
7. When you install Ubuntu, you’ll want to select Minimal Installation and uncheck the boxes for updates & third-party software

# Pics

![](final_setup.jpg)

![](first_setup.jpg)

![](miner1.JPG)

![](miner2.JPG)